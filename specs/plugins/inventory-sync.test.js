/* global describe, it, before, after */

'use strict'

let _conn = null
let _plugin = null
let _channel = null

const amqp = require('amqplib')
const reekoh = require('../../index.js')
const isEmpty = require('lodash.isempty')
const isEqual = require('lodash.isequal')
// const Broker = require('../../lib/broker.lib')

// preserving.. plugin clears env after init
const LOGGERS = 'logs1,logs2'
const ACCOUNT = 'demo.account'
const PLUGIN_ID = 'demo.dev-sync'
const EXCEPTION_LOGGERS = 'exlog1,exlog2'
const BROKER = 'amqp://guest:guest@127.0.0.1/reekoh'

describe('InventorySync Test', () => {
  // let _broker = new Broker()

  before('#test init', () => {
    process.env.CONFIG = '{}'
    process.env.ACCOUNT = ACCOUNT
    process.env.LOGGERS = LOGGERS
    process.env.EXCEPTION_LOGGERS = EXCEPTION_LOGGERS
    process.env.PLUGIN_ID = PLUGIN_ID
    process.env.BROKER = BROKER

    amqp.connect(process.env.BROKER).then((conn) => {
      _conn = conn
      return conn.createChannel()
    }).then((channel) => {
      _channel = channel
    }).catch((err) => {
      console.log(err)
    })
  })

  after('terminate connection', () => {
    _conn.close()
  })

  describe('#spawn', () => {
    it('should spawn the class without error', (done) => {
      try {
        _plugin = new reekoh.plugins.InventorySync()
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  describe('#events', () => {
    it('should rcv `ready` event', (done) => {
      _plugin.once('ready', () => {
        done()
      })
    })

    it('should rcv `sync` device event', (done) => {
      let dummyData = { 'operation': 'sync', '_id': 321, 'name': 'device-321' }
      _channel.sendToQueue(PLUGIN_ID, Buffer.from(JSON.stringify(dummyData)))

      _plugin.on('sync', () => {
        done()
      })
    })

    it('should rcv `adddevice` event', (done) => {
      let data = { 'operation': 'adddevice', 'device': { '_id': 321, 'name': 'device-321' } }
      _channel.sendToQueue(PLUGIN_ID, Buffer.from(JSON.stringify(data)))

      _plugin.on('adddevice', (device) => {
        if (isEmpty(device)) {
          done(new Error('Received empty device info'))
        } else {
          done()
        }
      })
    })

    it('should rcv `updatedevice` event', (done) => {
      let data = { 'operation': 'updatedevice', 'device': { '_id': 321, 'name': 'device-321' } }
      _channel.sendToQueue(PLUGIN_ID, Buffer.from(JSON.stringify(data)))

      _plugin.on('updatedevice', (device) => {
        if (isEmpty(device)) {
          done(new Error('Received empty device info'))
        } else {
          done()
        }
      })
    })

    it('should rcv `removedevice` event', (done) => {
      let data = { 'operation': 'removedevice', 'device': { '_id': 321, 'name': 'device-321' } }
      _channel.sendToQueue(PLUGIN_ID, Buffer.from(JSON.stringify(data)))

      _plugin.on('removedevice', (device) => {
        if (isEmpty(device)) {
          done(new Error('Received empty device info'))
        } else {
          done()
        }
      })
    })
  })

  describe('#syncDevice()', () => {
    it('should throw error if deviceInfo is empty', (done) => {
      _plugin.syncDevice('', []).then(() => {
        done(new Error('Expecting rejection. Check your test data.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Kindly specify the device information/details')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should throw error if deviceInfo doesnt have `_id` or `id` property', (done) => {
      _plugin.syncDevice({ foo: 'bar' }, []).then(() => {
        done(new Error('Expecting rejection. Check your test data.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Kindly specify a valid id for the device')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should throw error if deviceInfo doesnt have `name` property', (done) => {
      _plugin.syncDevice({ _id: 321 }, []).then(() => {
        done(new Error('Expecting rejection. Check your test data.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Kindly specify a valid name for the device')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should publish sync msg to queue', (done) => {
      _plugin.syncDevice({ _id: '321', name: 'foo' }, []).then(() => {
        setTimeout(done, 500)
      }).catch(done)
    })
  })

  describe('#removeDevice()', () => {
    it('should throw error if deviceId is empty', (done) => {
      _plugin.removeDevice('').then(() => {
        done(new Error('Expecting rejection. Check your test data.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Kindly specify a valid device identifier')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should publish remove msg to queue', (done) => {
      _plugin.removeDevice('321').then(() => {
        setTimeout(done, 500)
      }).catch(done)
    })
  })

  describe('#setState()', () => {
    it('should throw error if state is empty', (done) => {
      _plugin.setState(undefined).then(() => {
        done(new Error('Expecting rejection. Check your test data.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Please specify a valid state to set.')) {
          done(new Error('Return value did not match.'))
        } else {
          done()
        }
      })
    })

    it('should publish state msg to queue', (done) => {
      _plugin
        .setState(JSON.stringify({ lastSyncData: Date.now() }))
        .then(done)
        .catch(done)
    })
  })

  describe('#getState()', function () {
    this.timeout(8000)

    it('should request plugin state', (done) => {
      _plugin.getState().then(() => {
        done()
      }).catch((err) => {
        done(err)
      })
    })
  })

  describe('#log()', () => {
    it('should throw error if logData is empty', (done) => {
      _plugin.log('').then(() => {
        done(new Error('Expecting rejection. Check your test data.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Please specify a data to log.')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should send a log to logger queues', (done) => {
      _plugin.log('dummy log data').then(() => {
        done()
      }).catch(done)
    })
  })

  describe('#logException()', () => {
    it('should throw error if param is not an Error instance', (done) => {
      _plugin.logException('').then(() => {
        done(new Error('Expecting rejection. Check your test data.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Please specify a valid error to log.')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should send an exception log to exception logger queues', (done) => {
      _plugin.logException(new Error('test')).then(() => {
        done()
      }).catch(done)
    })
  })
})
