'use strict'

const EventEmitter = require('events').EventEmitter
let rp = require('request-promise')
let options
let PORT = 9091

class LatencyMonitor extends EventEmitter {
  constructor () {
    super()
    this.ts = {}
  }

  processStart () {
    let timestamp = Date.now()
    let timeTs = timestamp.toString()

    this.ts[timeTs] = timestamp

    let os = require('os-utils')

    os.cpuUsage(function (cpuPercentage) {
      let cpu = cpuPercentage
      let mem = 1 - os.freememPercentage()
      let memMb = ((os.totalmem() - os.freemem()) * 1024) / 1000000

      let data = {
        mem: mem,
        cpu: cpu,
        memMb: memMb
      }

      options = {
        method: 'POST',
        uri: `http://localhost:${PORT}/saturation`,
        body: data,
        json: true
      }
      rp(options)
        .catch(console.log)
    })

    options = {
      method: 'POST',
      uri: `http://localhost:${PORT}/traffic`,
      body: {},
      json: true
    }
    rp(options)
      .catch(console.log)

    return timeTs
  }

  processDone (tsKey) {
    let ts = Date.now() - this.ts[tsKey]

    options = {
      method: 'POST',
      uri: `http://localhost:${PORT}/latency`,
      body: { time: ts },
      json: true
    }
    rp(options)
      .catch(console.log)

    delete this.ts[tsKey]
  }
}

module.exports = LatencyMonitor
