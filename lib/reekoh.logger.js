'use strict'

const pino = require('pino')

class GlobalLogger {
  constructor (name) {
    let logStream = {
      write: (chunk) => {
        console.log(chunk)
      }
    }

    let pretty = pino.pretty({
      levelFirst: true,
      forceColor: true
    })

    pretty.pipe(process.stdout)

    this.logger = pino({
      name: name,
      safe: true
    }, (process.env.NODE_ENV) ? logStream : pretty)
  }

  info (msg) {
    this.logger.info(msg)
  }

  warn (msg) {
    this.logger.warn(msg)
  }

  error (msg) {
    this.logger.error(msg)
  }
}

module.exports = GlobalLogger
